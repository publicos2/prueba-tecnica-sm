<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** Database username */
define( 'DB_USER', 'wordpress' );

/** Database password */
define( 'DB_PASSWORD', 'wordpress' );

/** Database hostname */
define( 'DB_HOST', 'db' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Z,P9}63cyE /zJdh]y~J^K} ((.<hy8d#8x{=CY6/>chN9ltLL@p6vhlT9f)A.5o' );
define( 'SECURE_AUTH_KEY',  '6=~750;QQcf:dvYB7>fLU2gt<vDn1*50,WXC{Usu5.!;#EdGb)?y|g}Ix}j^oS$1' );
define( 'LOGGED_IN_KEY',    'Z/R0`dg)X[D_!>,wbCltHNb284D;*HcU4m25E<~Og351(h17MQ/T>I,}qvvFaJ~(' );
define( 'NONCE_KEY',        'hY/6_{pD3b|H:qxINfj=&W!MQZ2Oa2V&(:OIiJW<zM7X2cE6Z^dS}@]L=kjg*ORJ' );
define( 'AUTH_SALT',        'Q8RdLP|~E*rRM-wLy?YJk5O~qM ^TUPD:x>-]#~eyFD@= ]AZ9[5?`;|n!>P=W{v' );
define( 'SECURE_AUTH_SALT', '?G$vHgQ&rgV}c,*1IE8B0;CP!7%6CynO6 3Nn1=SYNdXZ}8rAlj|Q+n(dHD0eHx[' );
define( 'LOGGED_IN_SALT',   's/zG3q=#V)_ho|;}p|%tBY4lPHQ30r!9^xf$(]2}!pN~fR=Y.>lrO/P6C^5)Si.w' );
define( 'NONCE_SALT',       'B87_(CU Gw-Abr_6pE&$0mjoCZmv@;vIvO-:-imTk6qp{DvDe%T`:v&F77D)h^x5' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'Wd400_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
