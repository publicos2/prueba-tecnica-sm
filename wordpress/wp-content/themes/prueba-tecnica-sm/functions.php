<?php

function prueba_tecnica_style_enqueue_styles() {
    $styles = [

        [
            'name'=>'bootstrap',
            'dir_path'=>get_template_directory_uri() . '/assets/libs/bootstrap/css/bootstrap.min.css',
            'dep'=>[],
            'verion'=>'4.0'
        ],
        [
            'name'=>'style',
            'dir_path'=>get_stylesheet_uri(),
            'dep'=>['bootstrap'],
            'verion'=>'1.0'
        ],
        [
            'name'=>'lineawesome',
            'dir_path'=>get_template_directory_uri() . '/assets/fonts/line-awesome/css/line-awesome.min.css',
            'dep'=>['bootstrap'],
            'verion'=>'1.0'
        ]
    ];
    foreach ($styles as $style){
        wp_enqueue_style( $style['name'], $style['dir_path'],$style['dep'],$style['version'] );
    }
}
add_action( 'wp_enqueue_scripts', 'prueba_tecnica_style_enqueue_styles');

function themename_custom_logo_setup() {
    $defaults = array(
        'height'               => 100,
        'width'                => 400,
        'flex-height'          => true,
        'flex-width'           => true,
        'header-text'          => array( 'site-title', 'site-description' ),
        'unlink-homepage-logo' => true,
    );

    add_theme_support( 'custom-logo', $defaults );


    add_theme_support( 'post-thumbnails' );

    // Set post thumbnail size.
    set_post_thumbnail_size( 1200, 9999 );

    add_image_size( 'thumbnail-blog', 500, 500 );
}
add_action( 'after_setup_theme', 'themename_custom_logo_setup' );

function prueba_tecnica_registrar_menu() {
    register_nav_menu( 'menu-principal', 'Menu Principal' );
}
add_action( 'after_setup_theme', 'prueba_tecnica_registrar_menu' );

function prueba_tecnica_nav_class($classes, $item){
    $classes[] = 'nav-link';
    return $classes;
}
add_filter('nav_menu_css_class' , 'prueba_tecnica_nav_class' , 10 , 2);

function prueba_tecnica_registrar_sidebar() {
    register_sidebar( array(
        'name' =>'Sidebar',
        'id' => 'sidebar-raiola',
        'description' => 'Barra lateral del tema Raiola.',
        'before_widget' => '<div id="%1$s" class="card my-4 %2$s">',
        'after_widget'  => '</div></div>',
        'before_title'  => '<h5 class="card-header">',
        'after_title'   => '</h5>
        <div class="card-body">',
    ) );
}
add_action( 'widgets_init', 'prueba_tecnica_registrar_sidebar' );


function wpdocs_custom_excerpt_length( $length ) {
    return 12;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

function wpdocs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );
?>