<?php ?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap" rel="stylesheet">
    <?php
        wp_head();
    ?>
</head>

<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top custom-navbar">
    <div class="container">
        <?php
        the_custom_logo();
        ?>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <?php
            $menuraiola = array(
                'menu'            => 'menu-principal',
                'container'       => 'div',
                'container_class' => 'collapse navbar-collapse',
                'container_id'    => 'navbarResponsive',
                'menu_class'      => 'navbar-nav ml-auto',
                'menu_id'         => '',
                'echo'            => true,
                'fallback_cb'     => 'wp_page_menu',
                'before'          => '',
                'after'           => '',
                'link_before'     => '',
                'link_after'      => '',
                'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                'item_spacing'    => 'preserve',
                'depth'           => 0,
                'walker'          => '',
                'theme_location'  => '',
            );
            wp_nav_menu($menuraiola);
        ?>
    </div>
</nav>


<section class="main-header">
    <div class="container">
        <div class="info-header">
            <h2>Actualidad</h2>
            <h1>Digital</h1>
            <p>Tendencias, investigación y análisis del mundo del marketing y la tecnología, desarrollados por nuestros especialistas.</p>
            <hr>
        </div>
        <div class="main-info-image">
            <img src="<?php print get_template_directory_uri(); ?>/assets/images/line-next.png">
        </div>

    </div>
</section>
