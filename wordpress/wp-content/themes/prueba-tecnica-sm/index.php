<?php
get_header();
?>
<div class="content-filter">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-9">

            <?php
                $categories = get_categories( array(
                    'orderby' => 'name',
                    'order'   => 'ASC'
                ) );
                $html = '<option >Categoría</option>';
                foreach( $categories as $category ) {
                    $html .= '<option value="'.$category->term_id.'">'.$category->name.'</option>';
                }
            ?>
            <div class="form-group row filter-category">
                <label for="staticEmail" class="col-3 col-sm-2 col-form-label">Filtrar por:</label>
                <div class="col-9 col-sm-10">
                    <select class="form-control"> <?php print $html; ?></select>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container content-blog">
    <div class="row">
        <div class="col-12 col-md-9">
            <div class="row">

                <?php
                if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <div class="col-6 col-md-4">
                        <div class="card mb-4">
                            <div class="card-header" style="background-image: url('<?php global $post; print get_the_post_thumbnail_url($post->ID);?>')">
                            </div>
                            <div class="card-body">
                                <h2 class="card-title">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php the_title(); ?>
                                    </a>
                                </h2>
                                <p class="card-text"><?php the_excerpt(); ?></p>
                            </div>
                            <div class="card-footer text-muted">
                                <?php echo '<i class="las la-clock"></i> '. get_the_date(); ?>
                            </div>
                        </div>
                    </div>

                        <!--
                            AQUI VA EL HTML Y PHP DE LOS POSTS DEL HOME
                         -->

                <?php endwhile; else : ?>
                        <!-- SI NO SE CONSIGUE NINGÚN POST, SE RETORNA UN MENSAJE: -->
                        <p>Lo siento, no hemos encontrado ningún post.</p>
                <?php endif; ?>
                <nav class="raiola-paginacion">
                    <?php echo paginate_links(); ?>
                </nav>
            </div>
        </div>
        <div class="col-12 col-md-3">
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
